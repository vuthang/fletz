#! /bin/bash
images=(grafana/grafana quay.io/prometheus/prometheus:v2.0.0 prom/node-exporter:latest)

for image_name in "${images[@]}"
do
	docker ps | grep "$image_name" | gawk '{print $1}' | xargs docker stop || true
done
