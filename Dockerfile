FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ENV SPRING_PROFILE default
ADD /target/*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=${SPRING_PROFILE}","-jar","/app.jar"]

