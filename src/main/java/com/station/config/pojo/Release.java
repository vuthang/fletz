package com.station.config.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Release implements Serializable {

    private static final long serialVersionUID = 2891921400176821479L;

    private List<String> addon_plan_names;
    private App app;
    private String created_at;
    private String description;
    private String status;
    private String id;
    private String updated_at;
    private Integer version;
    private Boolean current;
    private String output_stream_url;
    private Slug slug;

    @Data
    class App {
        private String name;
        private String id;
    }

    @Data
    class Slug {
        private String id;
    }

    @Data
    class User {
        private String email;
        private String id;
    }
}
