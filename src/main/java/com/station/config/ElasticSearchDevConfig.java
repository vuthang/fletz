package com.station.config;

import com.station.config.interfaces.ElasticSearchConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

@Configuration
@Profile("dev")
public class ElasticSearchDevConfig implements ElasticSearchConfig {

    //Embedded Elasticsearch Server
    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchTemplate(nodeBuilder().local(true).node().client());
    }
}
