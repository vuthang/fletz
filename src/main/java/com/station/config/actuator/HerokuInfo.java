package com.station.config.actuator;

import com.station.config.pojo.Heroku;
import com.station.config.pojo.Release;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Component
public class HerokuInfo implements Endpoint<Heroku> {

    @Value("${heroku.api_key:}")
    private String tokenId;

    private Heroku data;

    @PostConstruct
    public void init() {
        createHeroukuData();
    }

    @Override
    public String getId() {
        return "heroku";
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isSensitive() {
        return false;
    }

    @Override
    public Heroku invoke() {
        loadData();
        return data;
    }

    private void loadData() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.heroku.com/apps/fletz/releases";

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept","application/vnd.heroku+json; version=3");
        headers.set("Authorization","Bearer " + tokenId);
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<Release[]> response
                = restTemplate.exchange(url, HttpMethod.GET, entity, Release[].class, new HashMap<>());

        if (response.getStatusCode().equals(HttpStatus.OK)) {
            for(Release r : response.getBody()) {
                if(r.getCurrent()) {
                    this.data.setRelease(r);
                    break;
                }
            }
        }
    }

    private void createHeroukuData() {
        this.data = Heroku.builder().build();
    }
}
