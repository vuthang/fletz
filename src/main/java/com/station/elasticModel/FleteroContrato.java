package com.station.elasticModel;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Data
@Builder
@Document(indexName = "contrato", type = "fletero_contrato")
public class FleteroContrato implements Serializable {

    @Id
    private String id;
    private String idFletero;
    private Double precioKilometro;
    private Boolean largaDistancia;
    private String clausulas;
}
