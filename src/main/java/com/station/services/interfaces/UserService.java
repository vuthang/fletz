package com.station.services.interfaces;

public interface UserService {

    boolean login(String username, String password);
}
