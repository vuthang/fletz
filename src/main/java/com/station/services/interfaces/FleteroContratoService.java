package com.station.services.interfaces;

import com.station.elasticModel.FleteroContrato;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Collection;

public interface FleteroContratoService {

    FleteroContrato save(FleteroContrato contrato);

    void delete(FleteroContrato contrato);

    FleteroContrato findOne(String id);

    Collection<FleteroContrato> findAll();

    Page<FleteroContrato> findByIdFletero(String idFletero, PageRequest pageRequest);

}
