package com.station.services.interfaces;

public interface AdminUserService {

    boolean isHerokuUser(String username, String password);
}
