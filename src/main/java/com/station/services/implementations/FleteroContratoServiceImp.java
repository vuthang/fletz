package com.station.services.implementations;

import com.google.common.collect.Lists;
import com.station.elasticModel.FleteroContrato;
import com.station.elasticRepositories.FleteroContratoRepository;
import com.station.services.interfaces.FleteroContratoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FleteroContratoServiceImp implements FleteroContratoService {

    @Autowired
    private FleteroContratoRepository fleteroContratoRepository;

    @Override
    public FleteroContrato save(FleteroContrato contrato) {
        return fleteroContratoRepository.save(contrato);
    }

    @Override
    public void delete(FleteroContrato contrato) {
        fleteroContratoRepository.delete(contrato);
    }

    @Override
    public FleteroContrato findOne(String id) {
        return fleteroContratoRepository.findOne(id);
    }

    @Override
    public Collection<FleteroContrato> findAll() {
        return Lists.newArrayList(fleteroContratoRepository.findAll());
    }

    @Override
    public Page<FleteroContrato> findByIdFletero(String idFletero, PageRequest pageRequest) {
        return fleteroContratoRepository.findByIdFletero(idFletero, pageRequest);
    }
}
