package com.station.fletz;

import com.google.gson.Gson;
import com.station.config.pojo.Release;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("gitlab-ci")
public class HerokuReleasesGitlabCITest {

    private Gson gson;

    @Value("${heroku.api_key:}")
    private String apiKey;

    @Before
    public void init() {
        gson = new Gson();
    }

    @Test
    public void loadReleasesTest() {
        if (!apiKey.isEmpty()) {

            RestTemplate restTemplate = new RestTemplate();
            String url = "https://api.heroku.com/apps/fletz/releases";

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept","application/vnd.heroku+json; version=3");
            headers.set("Authorization","Bearer " + apiKey);
            HttpEntity entity = new HttpEntity(headers);
            ResponseEntity<Release[]> response
                    = restTemplate.exchange(url, HttpMethod.GET, entity, Release[].class, new HashMap<>());
            assertTrue(response.getStatusCode().equals(HttpStatus.OK));
        }
    }
}
