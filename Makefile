pmd:
	mvn pmd:check -Dpmd.printFailingErrors=true

docker-ir:
	./command/docker-ir.sh

docker-ps:
	docker ps

metrics-up:
	docker-compose -f metrics/docker-compose.yml up -d

metrics-down:
	./command/metrics-down.sh
run:
	mvn spring-boot:run $(ARGS)

run-dev:
	mvn spring-boot:run -Drun.profiles=dev
